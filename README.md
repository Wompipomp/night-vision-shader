# Night vision shader image effect #

Written in HLSL/CG and Unity 5.2

Tested in Unity Editor with DX11, DX 9 and OpenGL

Features:    
- Ramp texture for colorisation   
- Noise   
- Lens distortion   
- Blur   
- Vignette  
- Downsampling

For more information and examples see website.